#!/usr/bin/python3
import os
import sys
import time
import json
import yeelight

if sys.platform == "linux":
    is_linux = True
else:
    is_linux = False
print("### Hyperion.ng - Yeelight - Bridge")
print("### Configurator")
print("\n### LED Devices")
print("Discovering yeelights...")
discovered = yeelight.discover_bulbs()
yeelights = []
modelsList = ['color', 'color1', 'color2', 'strip1']

for bulb in discovered:
    if bulb["capabilities"]["model"] in modelsList:
        yeelights.append(yeelight.Bulb(bulb["ip"]))

if len(yeelights) == 0:
    print(">> No Bulbs found. Please turn on all RGB Yeelight devices you want to use. Connect them to the same network. Set them to LAN mode in your yeelight app and try again.")
else:
    for bulb in yeelights:
        bulb.turn_off()

    time.sleep(1)

    path = sys.path[0]
    with open(path + "/bridge.default.config.json", "r") as f:
        config_file = json.load(f)

    bulbs = len(yeelights)
    print("%s devices found." % bulbs)

    for bulb in yeelights:
        bulb.turn_on()
        print(">> New Yeelight")
        print("\nGive a name of your choice to the light which turns on (e.g. right, left, top, bottom). Leave blank if you don't want to use it:")
        name = input()
        if not name == '':
            print("Corresponding to hyperion.ng webinterface, which LED number does this device have?")
            idx = input()
            bulb.set_name(name)
            config_file['yeelights'].append([int(idx), name])
        bulb.turn_off()
    print("All devices configured.")

    print("\n\n ### Kodi Settings")
    print("Kodi IP (default=127.0.0.1):")
    ip = input()
    if ip == '':
        ip = "127.0.0.1"
    config_file['kodi_ip'] = str(ip)
    print("Kodi remote port (default=80):")
    port = input()
    if port == '':
        port = 80
    config_file['kodi_port'] = int(port)
    print("Kodi username (default=''):")
    user = input()
    config_file['kodi_user'] = str(user)
    print("Kodi password (default=''):")
    pwd = input()
    config_file['kodi_pwd'] = str(pwd)

    print("\n\n ### Hyperion Settings")
    print("Hyperion UDPraw port (default=5568):")
    port = input()
    if port == '':
        port = 5568
    config_file['hyperion_port'] = port
    print("Hyperion rate (default=5):")
    rate = input()
    if rate == '':
        rate = 5
    config_file['hyperion_rate'] = rate

    with open(path + "/bridge.config.json", "w") as f:
        json.dump(config_file, f, indent=4, sort_keys=True)
    
    if is_linux:
        print("\n\n ### Service Setup")
        exec_file = "bridge.py"
        srv_file = "hyperion-yeelight-bridge.service"

        with open("%s/%s" % (path, srv_file), "r") as f:
            lines = f.read().split("\n")
            f.close()
        with open("%s/%s" %( path, srv_file), "w") as f:
            for idx, line in enumerate(lines):
                if line[:10] == 'ExecStart=':
                    lines[idx] = 'ExecStart=%s/%s'%(path, exec_file)
                    break
            srv_file_str = '\n'.join(lines)
            f.write(srv_file_str)
            f.close()

        cmd_srv_install = "sudo cp %s/%s /etc/systemd/system/%s && sudo systemctl enable %s && sudo systemctl restart %s" %(path, srv_file, srv_file, srv_file, srv_file)
        os.system(cmd_srv_install)
        print("Service setup done.")

    print("\n\n ### Configurator finished.")
    