#!/usr/bin/python3

import os
import sys
import socket
import signal
import time
import threading
import json
import numpy as np
import yeelight
from kodijson import Kodi, PLAYER_VIDEO

class KodiPlayerStatus:
    def __init__(self, ip="localhost", port=80, user="", pwd=""):
        # Variable
        self.playing = False
        self.t_stopped = time.time()
        # Kodi
        if (user or pwd) == "":
            self.kodi = Kodi("http://%s:%s/jsonrpc"%(ip, port))
        else:
            self.kodi = Kodi("http://%s:%s/jsonrpc"%(ip, port), user, pwd)
        # Thread
        t = threading.Thread(target=self._player_status)
        t.daemon = True
        self.mutex = threading.Lock()
        t.start()

    def is_playing(self):
        return self.playing

    def stopped_since(self, sec):
        dt = time.time() - self.t_stopped
        if dt > sec:
            return True
        else:
            return False
        
    def _player_status(self):
        t_prev = time.time()
        freq = 2.0
        try:
            while True:
                speed = self.kodi.Player.GetProperties(playerid=1, properties=["speed"])
                self.mutex.acquire()
                if speed['result']['speed'] > 0:
                    self.playing = True
                else:
                    self.playing = False
                    self.t_stopped = time.time()
                self.mutex.release()
                t = time.time()
                dt = t - t_prev
                if dt < (1.0/freq):
                    time.sleep(1.0/freq - dt)
                t_prev = t
        except KeyboardInterrupt:
            pass


class HyperionYeelightBridge:
    def __init__(self, kodi, yeelights, port=5568, rate=5):
        self.kodi = kodi # KodiPlayerStatus
        # Bulbs
        self.duration = 1.0/rate # in seconds
        self.bulbs_paused = False
        self.number_of_bulbs = self.get_number_of_bulbs(yeelights)
        self.bulbs = self.discover_bulbs(yeelights)
        self.init_bulbs(self.bulbs)
        # UDP
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(("", port))
        self.udp_data = []
        ## UDP-Thread
        t = threading.Thread(target=self._udp_receiver)
        t.daemon = True
        t.start()
        # Main
        self.main()

    def get_number_of_bulbs(self, yeelights):
        total_leds = 0
        for light in yeelights:
            if light[0] > total_leds:
                total_leds = light[0]
        total_leds += 1
        return total_leds

    def discover_bulbs(self, yeelights):
        print("discovering bulbs...")
        bulbs_list = []
        bulbs = yeelight.discover_bulbs()
        for bulb_id in yeelights:
            for bulb in bulbs:
                if bulb["capabilities"]["name"] == bulb_id[1]:
                    bulbs_list.append([bulb_id[0], yeelight.Bulb(bulb["ip"],
                                                                auto_on=True,
                                                                effect="smooth", 
                                                                duration=(self.duration*1000))])
        return bulbs_list

    def init_bulbs(self, bulbs):
        for bulb in bulbs:
            bulb[1].turn_off()
            time.sleep(0.1)
            bulb[1].stop_music()
            time.sleep(0.1)
            bulb[1].turn_on()
            time.sleep(0.1)
            bulb[1].start_music()
            time.sleep(0.1)

    def rgb_to_hsv(self, r, g, b):
        r, g, b = r/255.0, g/255.0, b/255.0
        mx = max(r, g, b)
        mn = min(r, g, b)
        df = mx-mn
        if mx == mn:
            h = 0
        elif mx == r:
            h = (60 * ((g-b)/df) + 360) % 360
        elif mx == g:
            h = (60 * ((b-r)/df) + 120) % 360
        elif mx == b:
            h = (60 * ((r-g)/df) + 240) % 360
        if mx == 0:
            s = 0
        else:
            s = (df/mx)*100
        v = mx*100
        return h, s, v
    
    def _udp_receiver(self):
        print("receiving udp data")
        while True:
            self.udp_data = self.socket.recvfrom(3*self.number_of_bulbs)
        self.socket.close()

    def udp_data_received(self):
        if len(self.udp_data) > 0:
            return True
        else:
            return False

    def get_rgb_split_from_udp_data(self, udp_data):
        rgb_array = bytearray(udp_data[0])
        rgb_split = np.split(np.array([*rgb_array]), self.number_of_bulbs)
        return rgb_split

    def set_bulbs_to_pause(self):
        if not self.bulbs_paused:
            self.bulbs_paused = True
            for bulb in self.bulbs:
                try:
                    bulb[1].set_hsv(0, 0, 100)
                except yeelight.BulbException:
                    continue

    def send_rgb_split(self, rgb_split):
        for bulb in self.bulbs:
            try:
                h, s, v = self.rgb_to_hsv(int(rgb_split[bulb[0]][0]),
                                            int(rgb_split[bulb[0]][1]),
                                            int(rgb_split[bulb[0]][2]))
                bulb[1].set_hsv(h, s, v)
            except yeelight.BulbException:
                continue

    def main(self):
        print("streaming rgb data to yeelight devices...")
        try:
            while True:
                if self.kodi.is_playing() and self.udp_data_received():
                    self.bulbs_paused = False
                    rgb_split = self.get_rgb_split_from_udp_data(self.udp_data)
                    self.send_rgb_split(rgb_split)
                else:
                    self.set_bulbs_to_pause()
                time.sleep(self.duration)

        except KeyboardInterrupt:
            print("Exit ...")

path = sys.path[0]
with open(path + "/bridge.config.json", "r") as f:
    loaded_file = json.load(f)

## kodi config
try:
    kodi_ip = loaded_file["kodi_ip"]
except:
    print("kodi_ip parameter missing. Check your config or rerun the configurator.py")
try:
    kodi_port = loaded_file["kodi_port"]
except:
    print("kodi_port parameter missing. Check your config or rerun the configurator.py")
try:
    kodi_user = loaded_file["kodi_user"]
except:
    print("kodi_user parameter missing. Check your config or rerun the configurator.py")
try:
    kodi_pwd = loaded_file["kodi_pwd"]
except:
    print("kodi_pwd parameter missing. Check your config or rerun the configurator.py")
## yeelight config
try:
    yeelights = loaded_file["yeelights"]
except:
    print("yeelights parameter missing. Check your config or rerun the configurator.py")
try:
    hyperion_port = loaded_file["hyperion_port"]
except:
    print("hyperion_port parameter missing. Check your config or rerun the configurator.py")
try:
    hyperion_rate = loaded_file["hyperion_rate"]
except:
    print("hyperion_rate parameter missing. Check your config or rerun the configurator.py")

kodi = KodiPlayerStatus(kodi_ip, kodi_port, kodi_user, kodi_pwd)
HyperionYeelightBridge(kodi, yeelights, hyperion_port, hyperion_rate)
    