# Hyperion.ng - Yeelight - Bridge

The Hyperion.ng Yeelight Bridge sends UDP packages of your Hyperion.ng instance to your yeelights.

## Requirement
* Python 3 (sudo pip3 install -r requirements.txt) 
  * numpy
  * kodi-json
  * yeelight
* Running instance of Hyperion.ng
  * LED Controller: 
    * Controller type: udpraw
    * RGB byte order: RGB
    * Target IP: IP of the system running the bridge (default=127.0.0.1)
    * Port: Port of the bridge (default=5568)
    ![LED Controller](images/led_controller.png)
  * LED Layout:
    * Like you want. 
    * **IMPORTANT**: Remember the 'LED numbers'. You have to specify them later in the configurator.py.
    ![LED Layout](images/led_layout.png)
* Running instance of Kodi with activated remote control
* Turn on your RGB yeelight bulbs you want to use, connect them to your network and activate LAN control for each bulb in the yeelight app


## Installation

```
cd ~/
git clone https://gitlab.com/tobimiller/hyperion-yeelight-bridge
cd hyperion-yeelight-bridge
sudo pip3 install -r requirements.txt
python3 configurator.py # follow the prompts
```